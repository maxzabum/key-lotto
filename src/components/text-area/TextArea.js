import React from "react";
import "./style.css";
function TextArea({ placeholder, onChange, disabled = false, value }) {
  return (
    <textarea
      className="text-area"
      placeholder={placeholder}
      onChange={onChange}
      disabled={disabled}
      value={value}
    />
  );
}

export default TextArea;
