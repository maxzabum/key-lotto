import logo from "./logo.svg";
import "./App.css";
import Button from "./components/button/Button";
import TextArea from "./components/text-area/TextArea";
import { useState } from "react";
import { splitToTwoNumberArray, subString } from "./helper";

function App() {
  const [numbers, setNumbers] = useState();
  const [lakNumber, setLakNumber] = useState();

  const handleOnChangeNumbers = (e) => {
    const number = e.target.value;
    setNumbers(number);
  };
  const handleOnCalFrontLak = () => {
    const sub = subString(numbers);
    const numberSet = splitToTwoNumberArray(sub) || [];
    let resLak = [];
    for (let i = 0; i < numberSet.length; i++) {
      const numb = numberSet[i];
      let lak = "";
      for (let j = 0; j < 10; j++) {
        lak = lak + `${j}${numb}-`;
      }
      resLak.push(lak);
    }
    setLakNumber(resLak.toString());
    console.log({ resLak, numberSet, sub });
  };
  const handleOnCalBackLak = () => {
    const sub = subString(numbers);
    const numberSet = splitToTwoNumberArray(sub) || [];
    let resLak = [];
    for (let i = 0; i < numberSet.length; i++) {
      const numb = numberSet[i];
      let lak = "";
      for (let j = 0; j < 10; j++) {
        lak = lak + `${numb}${j}-`;
      }
      resLak.push(lak);
    }
    console.log({ resLak, numberSet, sub });
    setLakNumber(resLak.toString());
  };
  return (
    <div className="main-container">
      <div className="laktak-container">
        <div className="header-container">เลขลากแตก</div>
        <div className="content-container">
          <TextArea onChange={handleOnChangeNumbers} value={numbers} />
          <div className="button-container">
            <Button label={"เบิ้ลหน้า"} onClick={handleOnCalFrontLak} />
            <Button label={"เบิ้ลหลัง"} onClick={handleOnCalBackLak} />
          </div>
          <TextArea disabled value={lakNumber} />
        </div>
      </div>
    </div>
  );
}

export default App;
