function subString(str) {
  return str.replace(/[^0-9]/g, "");
}

function splitToTwoNumberArray(str) {
  return str.match(/.{1,2}/g);
}
export { subString, splitToTwoNumberArray };
